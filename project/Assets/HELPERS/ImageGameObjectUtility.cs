using UnityEngine;
using System.Collections;

public class ImageGameObjectUtility  
{

    public static GameObject create(Texture aTex)
    {
        return (new ImageGameObjectUtility(aTex)).ParentObject;
    }

    public GameObject ParentObject { get; private set; }
    public GameObject PlaneObject { get; private set; }
    public Material PlaneMaterial { get; private set; }
	
	public Vector2 TextureOffset {
		get{
			return PlaneObject.renderer.material.mainTextureOffset;
		}
		set{
			PlaneObject.renderer.material.mainTextureOffset = value;
		}
	}
	public Vector2 TextureScale {
		get{
			return PlaneObject.renderer.material.mainTextureScale;
		}
		set{
			PlaneObject.renderer.material.mainTextureScale = value;
		}
	}
    public Vector2 BaseDimension { get; private set; }
    public Vector2 PixelDimension
    {
        get 
        { 
            Vector3 planeScale = PlaneObject.transform.localScale;
            return new Vector2(planeScale.x, planeScale.y); //techincally, you need to do something like convert units
        }
        set
        {
            PlaneObject.transform.localScale = new Vector3((value.x), (value.y), 0);
        }
    }

    public ImageGameObjectUtility(Texture aTex, System.Nullable<Vector2> aSize = null)
    {
        ParentObject = new GameObject("genImageObjectParent");
        PlaneObject = GameObject.CreatePrimitive(PrimitiveType.Quad);
		GameObject.DestroyImmediate(PlaneObject.GetComponent<MeshCollider>());
		//TODO use this one
        //PlaneObject = (GameObject)GameObject.Instantiate(ManagerManager.Manager.mReferences.mPlanePrefab);
		
		//TODO
        //PlaneMaterial = new Material(ManagerManager.Manager.mReferences.mDefaultCharacterShader);
		PlaneMaterial = new Material(Shader.Find("Custom/FlatGeneric"));
		
        PlaneObject.renderer.material = PlaneMaterial;
        set_new_texture(aTex, aSize);

        //PlaneObject.transform.rotation = Quaternion.AngleAxis(90, Vector3.right) * PlaneObject.transform.rotation;
        PlaneObject.transform.parent = ParentObject.transform;
    }

    public void set_new_texture(Texture aTex, System.Nullable<Vector2> aSize = null)
    {

        if (aSize == null)
        {
            if (aTex != null)
                BaseDimension = new Vector2(aTex.width, aTex.height);
            else
                BaseDimension = new Vector2(1, 1);
        }
        else
            BaseDimension = aSize.Value;
        PlaneObject.renderer.material.mainTexture = aTex;	
        PixelDimension = BaseDimension;
    }

    public void destroy()
    {
        GameObject.Destroy(ParentObject);
        GameObject.Destroy(PlaneObject);
    }

}
